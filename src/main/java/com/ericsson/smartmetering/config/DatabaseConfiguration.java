package com.ericsson.smartmetering.config;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories("com.ericsson.smartmetering.repository")
@EnableJpaAuditing(auditorAwareRef = "springSecurityAuditorAware")
@EnableTransactionManagement
public class DatabaseConfiguration {

	private final Logger log = LoggerFactory.getLogger(DatabaseConfiguration.class);
	
	 @Bean
	    public DataSource dataSource(){
	        //System.out.println(driverClass+" "+ url+" "+username+" "+password);
	        DriverManagerDataSource source = new DriverManagerDataSource();
	        /*source.setDriverClassName("org.postgresql.Driver");*/
	        source.setUrl("jdbc:postgresql://localhost:5432/Brunei_Online");
	        source.setUsername("postgres");
	        source.setPassword("admin");
	        System.out.println("source--------------"+source);
	        return source;
	    }

	    @Bean
	    public NamedParameterJdbcTemplate namedParameterJdbcTemplate(){
	        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(this.dataSource());
	        return namedParameterJdbcTemplate;
	    }
}
