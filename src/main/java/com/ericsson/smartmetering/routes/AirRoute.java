package com.ericsson.smartmetering.routes;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class AirRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		from("direct:sendAirRoute").to("http4://localhost:7080/Air")
				.process(getAir()).log("air data printed in console");
	}

	private Processor getAir() {
		return new Processor() {
			@Override
			public void process(Exchange exchange) throws Exception {
				System.out.println("body is ::" + exchange.getIn().getBody());
				System.out.println("header is ::" + exchange.getIn().getHeaders());
			}
		};
	}

}
